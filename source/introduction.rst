Introduction
============

Ce travail consiste à développer un tutoriel informatique en vulgarisant un sujet
complexe pour le rendre compréhensible par un étudiant du Gymnase intéressé par le sujet.

..  admonition:: LiveReload

    Trop cool ce LiveReload

Voici un exemple de code source

..  code-block:: python3

    from math import pickle
    
    print(math.pi)
    
et voici un exemple de HTML

..  code-block:: html

    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <h1>Mon titre exemple</h1>
        </body>
    </html>
    
Pourquoi ne pas mettre également un peu de mathématiques :

..  math::

    \sum_{i=1}^{N}
    i
    =
    \frac{
    N\cdot 
    (N+1)
    }
    {2}

